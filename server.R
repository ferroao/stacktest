library(shiny)
library(dplyr)
library(ggplot2)
library(scales)
library(gridExtra)
library(grid)
library(gtable)

r.tv.cantidad <- r.tv  %>%
    group_by(marca) %>%
    summarise(cantidad = length(marca))

tbl.precio.nuevo <- tableGrob(t(prettyNum(summary(r.tv$precio.nuevo), big.mark = ","))) 

tbl.precio.antes <- tableGrob(t(prettyNum(summary(r.tv$precio.antes), big.mark = ",")))

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
    
    output$plot1 <- renderPlot({

        g <- ggplot(r.tv.cantidad, aes(x=marca, y= cantidad)) + 
            geom_bar(stat = "identity", width = .7, fill= "lightblue") +
            labs(title = "",
                 x = "", y = "") +
            geom_text(aes(label=cantidad), vjust=-0.2, size = 06)  +
            theme(axis.text.x = element_text(colour="grey10",size=12,hjust=.5,vjust=.5,face="plain"),
                  axis.text.y = element_text(colour="grey10",size=12,hjust=1,vjust=0 ,face="plain"),  
                  axis.title.x = element_text(colour="grey40",size=14,angle=0,hjust=.5,vjust=0,face="plain"),
                  axis.title.y = element_text(colour="grey40",size=14,angle=90,hjust=.5,vjust=.5,face="plain"),
                  plot.title = element_text(vjust=2)
                  ) +
            scale_y_continuous(limits = c(0, 50))

        gg <- ggplotGrob(g)
        if(input$radio=="tbl.precio.nuevo"){
        g1 <- gtable::gtable_add_grob(gg, grobs = tbl.precio.nuevo 
                                      ,t=6, l=6, b=5, r=2
                                      )
        } else {
            g1 <- gtable::gtable_add_grob(gg, grobs = tbl.precio.antes 
                                          ,t=6, l=6, b=5, r=2
            )
        }
        grid::grid.draw(g1)
    })
})